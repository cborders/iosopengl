//
//  RendererWrapper.m
//  CTest
//
//  Created by Casey Borders on 2/16/18.
//  Copyright © 2018 Casey Borders. All rights reserved.
//

#import "RendererWrapper.h"
#import "../native/Render/Renderer.h"
#import "../native/AssetLoader.h"

@interface RendererWrapper()

@property (nonatomic, assign) Renderer *renderer;
@property (nonatomic, assign) Moo::AssetLoader *assetLoader;

@end

@implementation RendererWrapper

- (id) init {
    self = [super init];
    
    if (self) {
        self.assetLoader = new Moo::AssetLoader(nullptr);
        self.renderer = new Renderer(self.assetLoader);
    }
    
    return self;
}

- (void) initGL {
    self.renderer->initGL();
}

- (void) resizeWidth:(CGFloat) width andHeight:(CGFloat)height {
    self.renderer->resize(int(width), int(height));
}

- (void) draw:(NSTimeInterval) deltaTime {
    self.renderer->draw(float(deltaTime));
}

- (void) touchDownWith:(int) pointerId at:(float)x and:(float)y {
    self.renderer->touchDown(pointerId, x, y);
}

- (void) touchMoveWith:(int) pointerId at:(float)x and:(float)y
{
    self.renderer->touchMove(pointerId, x, y);
}

- (void) touchUpWith:(int) pointerId at:(float)x and:(float)y {
    self.renderer->touchUp(pointerId, x, y);
}

@end
