//
//  RendererWrapper.h
//  CTest
//
//  Created by Casey Borders on 2/16/18.
//  Copyright © 2018 Casey Borders. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CGBase.h>

@interface RendererWrapper : NSObject

- (void) initGL;
- (void) resizeWidth:(CGFloat) width andHeight:(CGFloat)height;
- (void) draw:(NSTimeInterval) deltaTime;

- (void) touchDownWith:(int) pointerId at:(float)x and:(float)y;
- (void) touchMoveWith:(int) pointerId at:(float)x and:(float)y;
- (void) touchUpWith:(int) pointerId at:(float)x and:(float)y;

@end
