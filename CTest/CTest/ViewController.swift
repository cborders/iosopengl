//
//  ViewController.swift
//  CTest
//
//  Created by Casey Borders on 2/16/18.
//  Copyright © 2018 Casey Borders. All rights reserved.
//

import UIKit
import GLKit

class ViewController: GLKViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
