//
//  GLView.swift
//  CTest
//
//  Created by Casey Borders on 2/16/18.
//  Copyright © 2018 Casey Borders. All rights reserved.
//

import UIKit
import GLKit

class GameView: GLKView {

    var initDone = false
    var renderer = RendererWrapper()
    var lastTimeUpdate : Date?
    
    override init (frame : CGRect) {
        super.init(frame : frame)
        initGL()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder : aDecoder)
        initGL()
    }
    
    func initGL() {
        self.context = EAGLContext(api: .openGLES2)!
    }
    
    override func draw(_ rect: CGRect) {
        if(!initDone) {
            renderer.initGL();
            let scale = UIScreen.main.scale
            renderer.resizeWidth(self.frame.size.width * scale, andHeight: self.frame.size.height * scale)
            lastTimeUpdate = Date()
            initDone = true
        }

        let now = Date()
        let interval = now.timeIntervalSince(lastTimeUpdate!)
        renderer.draw(interval)
        lastTimeUpdate = now
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for (index, touch) in touches.enumerated() {
            let position = touch.location(in: self)
            self.renderer.touchDown(with: Int32(index), at: Float(position.x), and: Float(position.y))
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        for (index, touch) in touches.enumerated() {
            let position = touch.location(in: self)
            self.renderer.touchMove(with: Int32(index), at: Float(position.x), and: Float(position.y))
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for (index, touch) in touches.enumerated() {
            let position = touch.location(in: self)
            self.renderer.touchUp(with: Int32(index), at: Float(position.x), and: Float(position.y))
        }
    }
}
